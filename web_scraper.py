from numpy import NaN
from selenium.common import exceptions
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from time import sleep
import time
import csv
#import unidecode
#import pyautogui

#screenWidth, screenHeight = pyautogui.size()
#currentMouseX, currentMouseY = pyautogui.position()
#print(currentMouseX, currentMouseY)

slow_pc = 1

#returns true if string is a float, false otherwise
def is_float(maybe_num):
  try:
    float(maybe_num)
    return True
  except:
    return False

def is_in_list(list, element):
  for i in range(len(list)):
      if element == list[i]:
          return True
  return False

#extracts row values
def get_row_vals(row, write_header, division_name, player_position, write_div_name):
  data = []
  the_href = NaN
  cells = row.findChildren('td')
  #this is the header
  if cells == []:
    #header is in th cells
    if write_header == 0:
      #we write the header
      cells = row.findChildren('th')
      for cell in cells:
        value = cell.string
        data.append(value)
      if write_div_name == 1:
        data.append('Division Name')
        data.append('Player Position')
      data.append('Player href')
  else:
    #these are the column values
    for cell in cells:
      if cell.find("img") != None:
        img = cell.find("img")
        value = img['alt']
        data.append(value)
      
      else:
        if cell.find("a") != None:
          #print("Cell a :",cell.find("a"))
          a = cell.find("a")
          the_href = a["href"]
          #the_href = cell['href']
          #print("LA HREFFFFFFFFFFFFFFFFFFFFF : ",the_href)
          #data.append(value)
        value = cell.string
        if is_float(value):
          data.append(float(value))
        elif value == "-":
          data.append(NaN)
        else:
          data.append(value) #unidecode.unidecode(value))
    if write_div_name == 1:
      #print("I am appending the div name AAAAAAAAAAAAAAAAAA")
      data.append(division_name)
      data.append(player_position)
    data.append(the_href)
  
  return data

def scrape_tables(write_div_name, div_name): #write_div_name #head #num_tables, 
  positions = ['Goalkeeper', 'Defender','Midfielder','Attacker']
  sleep(1)
  f = open('unfinished_data.csv', 'w', encoding='UTF8')
  # create the csv writer
  writer = csv.writer(f)
  head = 0
  for pos in range(0,4):
    #select player position on
    driver.find_element(by=By.XPATH, 
      value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[2]/div[2]/div/div/label[" + str(pos + 1) + "]/span").click()
    #apply button
    driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[7]/button[1]").click()
    sleep(1)
    web_html = driver.page_source
    #find the table we want to look at, got_tables = number of times it tries getting the table
    got_table = 20
    while got_table > 0:
      try:
        bs = BeautifulSoup(web_html, features='lxml')
        table = bs.find_all('table')[0]
        rows = table.findChildren('tr')
        for row in rows:
          cells = row.findChildren('td')
          print("table" + str(cells[0]) + "\n\n\n")
          exit(0)
      except IndexError:
        #print("c'est pas bon")
        #sleep(0.2)
        got_table -= 1
    #bs = BeautifulSoup(web_html, features='lxml')
    find_num_buttons = bs.find_all('button')
    #print("NUMBER OF BUTTONSSSSSSSSSSSSSSSSSSS :",len(find_num_buttons))
    num_buttons = range(1,len(find_num_buttons) + 1)
    num_tables = 0
    #number of table pages
    for button in range(len(find_num_buttons)):
      #print("button number " + str(button) + " is : " + str(find_num_buttons[button].string))
      if is_float(find_num_buttons[button].string):
        if is_in_list(num_buttons,int(find_num_buttons[button].string)) and int(find_num_buttons[button].string) > num_tables:
          num_tables = int(find_num_buttons[button].string)
    #driver.close
    if num_tables == 0:
      #this means table has not been loaded, we try once more to find number of table pages
      #apply button
      driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[7]/button[1]").click()
      sleep(2)
      find_num_buttons = bs.find_all('button')
      #print("NUMBER OF BUTTONSSSSSSSSSSSSSSSSSSS :",len(find_num_buttons))
      num_buttons = range(1,len(find_num_buttons) + 1)
      num_tables = 0
      for button in range(len(find_num_buttons)):
        if is_float(find_num_buttons[button].string):
          if is_in_list(num_buttons,int(find_num_buttons[button].string)) and int(find_num_buttons[button].string) > num_tables:
            num_tables = int(find_num_buttons[button].string)  
    if num_tables == 0:
      exit(0)
    #print("SPECITAL NUMBER OF TABLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEES",num_tables)

    if pos > 0:
      f = open('unfinished_data.csv', 'a', encoding='UTF8')
      # create the csv writer
      writer = csv.writer(f)
    #wdn = write_div_name
    for pl in range(num_tables):
        #print("@@@ get the players from table",pl)
        web_html = driver.page_source
        #df = pd.read_html(web_html)
        
        #WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/table/tbody/tr[1]/td[3]")))  #(By.ID, "waitCreate")))
        #find the table we want to look at, got_tables = number of times it tries getting the table
        got_table = 20
        while got_table > 0:
          try:
            bs = BeautifulSoup(web_html, features='lxml')
            table = bs.find_all('table')[0]
          except: #IndexError:
            sleep(0.5)
          got_table -= 1
        rows = table.findChildren('tr')
        for row in rows:
          if len(rows) > 21:
            print("row length problem, number of rows =",len(rows))
            driver.close()
            exit(0)
          data = get_row_vals(row, head, div_name, positions[pos], write_div_name) #wdn) #English Premier League
          if data != []:
            writer.writerow(data)
        #wdn = 0
        head = 1
        if pl <= (num_tables - 2):
          #click on "next" button
          try:
            if driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[8]/button[2]") != "":
              try:
                #print("driver found this : ",driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[8]/button[2]"))
                driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[8]/button[2]").click()
              except exceptions.NoSuchElementException:
                print("Cant click on 'next' button to load table number " + str(pl + 2) + " out of " + str(num_tables))# + "\nWaiting 2 seconds and retrying")
                #sleep(2)
                #driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[8]/button[2]").click()
          except exceptions.NoSuchElementException:
            driver.close()
            driver.quit()
            exit(0)
            script = "window.scrollTo(0, 0)"
            driver.execute_script(script)
            sleep(0.4)
            script = "window.scrollTo(0, " + str(scroll2[div]) + ")"
            driver.execute_script(script)
            sleep(1)
            print("Error finding 'next' button, reinitializing")
            #apply button
            driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[7]/button[1]").click()
            sleep(0.5)
            #get back to page we wanted
            print("Getting back to page " + str(pl + 2) + " out of " + str(num_tables) + "\nI have to click " + str(pl + 1) + " times")
            for i in range(0,pl + 1):
              driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[8]/button[2]").click()
              print("click number ", i + 1)
              sleep(0.2)
          sleep(0.5)
    f.close()
    #select player position off
    driver.find_element(by=By.XPATH, 
        value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[2]/div[2]/div/div/label[" + str(pos + 1) + "]/span").click()
    #apply button
    driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[7]/button[1]").click()

def csv_join(writetype,all_data_csv_is_empty):
  #reading raw data csv
  df_data = pd.read_csv('unfinished_data.csv')

  #if no data has been written in the final csv file
  if writetype == 1:
    #delete column with player position on table
    df_data = df_data.drop('#',axis=1)
    #print("after changing stuff\n",df_data.head())
  #if the final csv already has data in it
  elif writetype == 2:
    #delete column with player position on table
    #delete duplicate columns
    df_data = df_data.drop('#',axis=1)
    df_data = df_data.drop('Team',axis=1)
    df_data = df_data.drop('Name',axis=1)
    
  if all_data_csv_is_empty == 1:
    #print("all data is empty")
    f = open('all_data.csv', 'w', encoding='UTF8')
    #writing the clean csv into all_data.csv
    df_data.to_csv(f, index=False)
    f.close()
  else:
    f = open('data.csv', 'w', encoding='UTF8')
    #writing the clean csv into data.csv
    df_data.to_csv(f, index=False)
    f.close()
    #joining the data.csv to all_data.csv
    a = pd.read_csv('all_data.csv')
    b = pd.read_csv('data.csv')
    merged = a.merge(b, on='Player href') #on=['Team','Name']) #on='Name'
    merged.to_csv('all_data.csv', index=False)



option = Options()
option.headless = False
#extension_path = "/usr/lib/firefox-addons/plugins/image_block-5.0-fx.xpi"

#firefox_profile = webdriver.FirefoxProfile()
#firefox_profile.set_preference('permissions.default.image', 2)
#firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
#option.add_extension()
#profile_path = r"/home/milan/.mozilla/firefox/2twm3ryn.default"
#option.set_preference('profile',profile_path)
driver = webdriver.Firefox(options=option) #firefox_profile=firefox_profile,
#driver = webdriver.Firefox()
#driver.install_addon(extension_path,temporary=True)
driver.maximize_window()

#website we scrape
website = "https://www.sofascore.com/tournament/football/"
#choose division
division_list = ["france/ligue-1/34", "england/premier-league/17", "italy/serie-a/23", "germany/bundesliga/35", "spain/laliga/8", "portugal/primeira-liga/238", "netherlands/eredivisie/37", "mexico/liga-mx-clausura/11620", "mexico/liga-mx-apertura/11621", "russia/premier-liga/203", "belgium/pro-league/38", "turkey/super-lig/52", "switzerland/super-league/215","scotland/premiership/36", "croatia/1-hnl/170"] 
#
#usa/mls/242, "brazil/brasileiro-serie-a/325", japan/jleague/196, "austria/bundesliga/45", "china/cfa-super-league/649", colombia/primera-a-clausura/11536, 
#too big : "argentina/liga-profesional-de-futbol/155", "england/championship/18", 
division_name_list = ['French Ligue 1', 'English Premier League', 'Italian Serie A', 'German Bundesliga', 'Spanish La Liga', 'Portuguese Primeira Liga', 'Dutch Eredivisie', 'Mexican Liga MX Clausura', 'Mexican Liga MX Apertura', 'Russian Premier Liga', 'Belgian Jupiler League', 'Turkish Super Lig', 'Swiss Super League', 'Scottish Premiership', 'Croatian National League'] 
#
#US MLS, 'Brazilerio Serie A', Japanese J. League, 'Austrian Bundesliga', 
#too big : 'Argentinian Professional Football League', 'English Championship', 

scroll1 = [2020,2020,2020,1900,2020,1900,1900,1900,2500,1850,1900,2020,1720,1780,1750]
#
#too big : 2200,2140,
scroll2 = [2080,2080,2080,2000,2080,2000,2000,2000,2650,1950,2180,2000,2080,1780,1800]
#
#too big : 2250,1850,

start_time = time.time()
last_time = start_time

for div in range(0,len(division_list)): #/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[11]/div/div/div[1]/a[6]
  #print("number of divisions :",len(division_list))
  division_name = division_name_list[div]
  current_division = website + division_list[div]
  print(division_name)
  driver.get(current_division)

  #sleep(slow_pc * 3)
  #pyautogui.click(1860, 130)
  #driver.refresh()

  #we scroll to load the table
  sleep(slow_pc * 2)
  driver.execute_script("window.scrollTo(0, 1080)")
  sleep(slow_pc * 1)
  script = "window.scrollTo(0, " + str(scroll1[div]) + ")"
  driver.execute_script(script) #2020 #1900
  sleep(slow_pc * 1)

  #pyautogui.click((0.267*screenWidth), (0.4518*screenHeight))

  #DETAILED STATS
  driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[1]/a[6]").click()
  #scroll to avoid button being hidden
  script = "window.scrollTo(0, " + str(scroll2[div]) + ")"
  driver.execute_script(script) #2080 #2000
  sleep(slow_pc * 0.5)

  if div == 0:
    for i in range(1,7):
      #take off current stats
      driver.find_element(by=By.XPATH, 
      value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[7]/button[1]").click()
    #take off current player positions
    for i in range(1,5):
      driver.find_element(by=By.XPATH, 
          value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[2]/div[2]/div/div/label[" + str(i) + "]/span").click()
  else:
    for i in range(1,4):
      #take off current stats
      driver.find_element(by=By.XPATH, 
      value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[7]/button[1]").click()

  #Scraping System
  #collapsable
  num_checkboxes = [24, 12, 17, 15, 17]
  write_div_name = 1
  all_data_csv_is_empty = 1
  for collapsable in range(0,5):
    #print("ALLLLL DATAAAAAAAAAAA :", all_data_csv_is_empty)
    #print("Checkbox group :",collapsable + 1)
    #collapsable xpath
    col_xpath = "/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[1]/div[" + str(collapsable + 1) + "]"
    #col_xpath = "/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[1]/div[" + str(1) + "]"
    driver.find_element(by=By.XPATH, value=col_xpath).click()
    sleep(slow_pc * 0.5)
    # open the file in the write mode
    #f = open('unfinished_data.csv', 'w', encoding='UTF8')
    # create the csv writer
    #writer = csv.writer(f)
    #num_checkboxes = 24
    head = 0
    how_many_to_uncheck = 0
    for i in range(1,num_checkboxes[collapsable]):
      if i != 1 and ((i % 6) == 1 or i == (num_checkboxes[collapsable] - 1)): #i == 7 or i == 13 or i == 19 or i == 23: #modulo(6,i) = 1
        if i == (num_checkboxes[collapsable] - 1) and collapsable == 0:
          #click on the rating statistic checkbox
          #print("imma click on checkbox number", i)
          my_xpath = "/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[" + str(collapsable + 2) + "]/div/label[" + str(i) + "]/span"
          driver.find_element(by=By.XPATH,value=my_xpath).click()
          how_many_to_uncheck += 1
        #hide collapsable
        driver.find_element(by=By.XPATH, 
        value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[1]/div[" + str(collapsable + 1) + "]").click()
        sleep(slow_pc * 0.5)

        #apply button
        driver.find_element(by=By.XPATH, value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[7]/button[1]").click()
        sleep(slow_pc * 0.5)

        head = 0
        
        #doing the division player stats scraping
        scrape_tables(write_div_name, division_name) #head, #int(num_tables), 
        if head == 0:
          head = 1

        if write_div_name == 1:
          write_div_name = 0

        if i == 7 and collapsable == 0:
          #first time writting (write header)
          csv_join(1, all_data_csv_is_empty)
          all_data_csv_is_empty = 0
          #print("NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW :",all_data_csv_is_empty)
        else:
          #not first time writing (write players)
          csv_join(2,all_data_csv_is_empty)
          #exit(0)

        for j in range(0,how_many_to_uncheck): #range(1,(num_checkboxes[collapsable] - i)):
          #take off current stats
          driver.find_element(by=By.XPATH, 
          value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[7]/button[1]").click()
        how_many_to_uncheck = 0
        #collapsable
        driver.find_element(by=By.XPATH,
        value="/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[1]/div[" + str(collapsable + 1) + "]").click()
        sleep(slow_pc * 0.5)
      
      if (num_checkboxes[collapsable] - i) > 1: #i != 23:
        #click on particular statistic checkbox
        my_xpath = "/html/body/div[1]/main/div/div[2]/div[1]/div[1]/div[10]/div/div/div[2]/div/div/div[6]/div/div[" + str(collapsable + 2) + "]/div/label[" + str(i) + "]/span"
        driver.find_element(by=By.XPATH,value=my_xpath).click()
        how_many_to_uncheck = how_many_to_uncheck + 1
    
  # The current lap-time
  div_time = round((time.time() - last_time)/60.0, 2)
  # Total time elapsed since the timer started
  total_time = round((time.time() - start_time)/60.0, 2)
  print("Total time elapsed : " + str(total_time) + "min")
  print("This division time taken : " + str(div_time) + "min")
  # Updating the previous total time
  last_time = time.time()

  #move data to all_player_stats.csv
  if div == 0:
    df = pd.read_csv('all_data.csv', encoding='UTF8')
    print(df.shape[0], df.shape[1])
    #copy all_data into empty all_player_stats
    f = open('all_player_stats.csv', 'w', encoding='UTF8')
    #writing the clean csv into all_data.csv
    df.to_csv(f, index=False)
    f.close()
  else:
    df = pd.read_csv('all_data.csv', encoding='UTF8')
    print(df.shape[0], df.shape[1])
    #add new division player stats to all_player_stats
    f = open('all_player_stats.csv', 'a', encoding='UTF8')
    writer = csv.writer(f)
    for i in range(df.shape[0]):
      #if i == 0:
      #  print(df.iloc[i][:])
      writer.writerow(df.iloc[i][:])
    f.close()
  #driver.close()
  #sleep(5)
driver.quit()
exit(0)

#print("\n".join([img['alt'] for img in bs.find_all('img', alt=True)]))

"""from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
import pandas as pd
from time import sleep
import requests

option = Options()
option.headless = False
option.add_experimental_option("detach", True)
s = Service(executable_path="/usr/bin/chromedriver")
driver = webdriver.Chrome(service=s, options=option) #executable_path="/usr/bin/chromedriver") #options=option)
driver.maximize_window()
url = "https://www.sofascore.com/tournament/football/england/premier-league/17"
driver.get(url)

#driver.quit()"""